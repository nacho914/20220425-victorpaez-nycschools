package com.schools.newyork.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.schools.newyork.R
import com.schools.newyork.Util
import com.schools.newyork.models.model.*

import com.schools.newyork.adapters.SchoolsAdapter
import com.schools.newyork.viewmodels.ViewModel
import io.reactivex.plugins.RxJavaPlugins
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {


    lateinit var recycleView: RecyclerView
    var schoollist = ArrayList<School>()
    lateinit var adapter: SchoolsAdapter
    lateinit var textView: TextView
    lateinit var progressbarlayout: View
    lateinit var modelView: ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)




        castViews()
        modelView = ViewModelProviders.of(this).get(ViewModel::class.java)

        button.setOnClickListener {

                modelView.getData()
            progressbarlayout.visibility=View.VISIBLE
        }

        val nameObserver = Observer<ArrayList<School>> { list ->

            schoollist.clear()
            schoollist.addAll(list)
            adapter.notifyDataSetChanged()
            progressbarlayout.visibility = View.INVISIBLE
        }

        modelView.getObservable().observe(this, nameObserver)



        adapter = SchoolsAdapter(
            this,
            schoollist
        ){ it->
            val intent = Intent(this, Detail::class.java)
            val gson = Gson()
            val school = gson.toJson(it)
            intent.putExtra("school", school)
            startActivity(intent)


        }
        recycleView.adapter = adapter




        RxJavaPlugins.setErrorHandler { throwable ->
            progressbarlayout.visibility = View.INVISIBLE
            Toast.makeText(
                this,
                Util.showError(throwable), Toast.LENGTH_LONG
            ).show()
            Log.e(this@MainActivity.javaClass.name, throwable.message.toString())

        }


    }


    private fun castViews() {

        recycleView = findViewById<RecyclerView>(R.id.recyclerView)
        recycleView.setHasFixedSize(true)
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recycleView.setLayoutManager(linearLayoutManager)

        progressbarlayout = findViewById(R.id.progressbarlayout)

    }


    override fun onResume() {
        super.onResume()
        adapter.notifyDataSetChanged()
    }


}