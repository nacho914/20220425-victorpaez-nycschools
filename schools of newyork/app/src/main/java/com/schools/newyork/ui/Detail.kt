package com.schools.newyork.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.schools.newyork.R
import com.schools.newyork.models.model
import com.schools.newyork.viewmodels.DetailViewModel
import kotlinx.android.synthetic.main.activity_detail.*


class Detail : AppCompatActivity() {
    var school: model.School = model.School()
    lateinit var modelView: DetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        modelView = ViewModelProviders.of(this).get(DetailViewModel::class.java)




        try {
            val gson = Gson()
            school = gson.fromJson<model.School>(
                intent.getStringExtra("school"), model.School::class.java
            )
            modelView.getData(school.dbn)

        } catch (e: Exception) {
            Log.e("#ERROR", e.message!!)
        }

        val nameObserver = Observer<ArrayList<model.SAT>> {


            schName.text = school.schoolName
            overview.text = school.overviewParagraph
            districtnum.text = school.councilDistrict
            math.text = it.first().satMathAvgScore
            readin.text = it.first().satCriticalReadingAvgScore
            writin.text =it.first().satWritingAvgScore
            takersnumber.text = it.first().numOfSatTestTakers


        }

        modelView.getObservable().observe(this, nameObserver)


    }


}