package com.schools.newyork.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.schools.newyork.WebServices.NetworkHelper
import com.schools.newyork.models.model.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


class ViewModel(context: Application) : AndroidViewModel(context) {

    private var schoolList = MutableLiveData<ArrayList<School>>()

    private val Disposable: CompositeDisposable? = CompositeDisposable()

    fun getData() {

        NetworkHelper.createGitHubAPI().getSchools()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                Disposable!!.add(it)
            }
            .doOnError {
                var t = 0
            }
            .doOnNext { data ->

                schoolList.value = data as ArrayList<School>?

            }
            .subscribe()
    }


    fun getObservable(): MutableLiveData<ArrayList<School>> {
        return schoolList
    }


    override fun onCleared() {
        Disposable!!.clear()
    }

}