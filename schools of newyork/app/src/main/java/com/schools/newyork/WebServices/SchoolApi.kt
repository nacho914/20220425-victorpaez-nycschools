package com.schools.newyork.WebServices

import com.schools.newyork.models.model.*
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


interface SchoolApi {


    @GET("resource/s3k6-pzi2.json")
    fun getSchools(): Observable<List<School>>


    @GET("resource/f9bf-2cp4.json")
    fun getSchoolDetail(@Query("dbn") number: String?): Observable<List<SAT>>
}



