package com.schools.newyork.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.schools.newyork.R
import com.schools.newyork.models.model.*
import kotlinx.android.synthetic.main.elementview.view.*


class SchoolsAdapter(
    val context: Context,
    val schools: ArrayList<School>,
    private val clickListener: (School) -> Unit

) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun getItemCount(): Int {
        return schools.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.elementview, parent, false)
        )

    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        (holder as ViewHolder).bind(schools[position], context)
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(school: School?, context: Context) {


            school?.let {
                itemView.setOnClickListener {
                    clickListener(school)
                }
                with(school){
                    itemView.name.text = this.schoolName
                    itemView.council.text ="District"  + this.councilDistrict
                    itemView.district.text = this.neighborhood
                }

            }

        }

    }

}

